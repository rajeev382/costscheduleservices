package io.cloudensure.costschedule.api;

import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import Constant.Constant;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ecs.AmazonECS;
import com.amazonaws.services.ecs.AmazonECSClientBuilder;
import com.amazonaws.services.ecs.model.ClientException;
import com.amazonaws.services.ecs.model.ContainerOverride;
import com.amazonaws.services.ecs.model.Failure;
import com.amazonaws.services.ecs.model.KeyValuePair;
import com.amazonaws.services.ecs.model.RunTaskRequest;
import com.amazonaws.services.ecs.model.RunTaskResult;
import com.amazonaws.services.ecs.model.StopTaskRequest;
import com.amazonaws.services.ecs.model.TaskOverride;


public class aws {
	public AmazonEC2 getAWSObject() throws IOException, ParseException {

		AmazonEC2 ec2 = null;
		try {
			System.out.println("Instance operation");
			BasicAWSCredentials awsCreds = new BasicAWSCredentials(Constant.access_key, Constant.secret_key);

			//AmazonECS client = AmazonECSClientBuilder.standard().withRegion("us-east-1").build();
//			.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).
			//ec2Client = AmazonEC2ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(temporaryCredentials)).withRegion(region.getName()).build();
			ec2 = AmazonEC2ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();

		} catch (Exception e) {
			System.out.println("EXCEPTION" + e);
		}
		return ec2;
}
	public void awsStartInstance(AmazonEC2 ec2,String instance_id) throws IOException, ParseException {
		StartInstancesRequest request = new StartInstancesRequest()
			    .withInstanceIds("i-01222da8a6b836897");
		System.out.println(ec2.startInstances(request));
	}
	public void awsStopInstance(AmazonEC2 ec2,String instance_id) throws IOException, ParseException {
		StopInstancesRequest request = new StopInstancesRequest()
			    .withInstanceIds("i-01222da8a6b836897");
		System.out.println(ec2.stopInstances(request));
	}
}	

package io.cloudensure.costschedule.dboperation;

import java.text.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Date;
import io.cloudensure.costschedule.api.aws;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import com.amazonaws.services.ec2.AmazonEC2;
import Constant.Constant;

public class dboperation {
	Helper logger = new Helper();
	
	public Connection connect() throws SQLException, ClassNotFoundException {
		Class.forName("java.sql.DriverManager");
		Connection connection = DriverManager.getConnection(Constant.connsectionString, Constant.dbUser,
				Constant.password);

		System.out.printf("Connected to PostgreSQL database! \n");
		return connection;
	}
	public String instanceStartStopOperation(Connection conn) throws SQLException {

		String instance_id = null;
		String SQL = "SELECT * FROM cen.cost_schedule_recommendation where scheduled = true and is_deleted = false";
		try (PreparedStatement pstmt = conn.prepareStatement(SQL)) {
			ResultSet resultSet = pstmt.executeQuery();
			String start_date = null;
			String end_date = null;
			String start_hour = null;
			String end_hour = null;
			String start_day = null;
			String end_day = null;
			aws awsoperation = new aws(); 
			AmazonEC2 ec2 = awsoperation.getAWSObject();
			while (resultSet.next()) {
				instance_id = resultSet.getString("instance_id");
				start_date = resultSet.getString("start_date");
				end_date = resultSet.getString("end_date");
				start_hour = resultSet.getString("start_hour");
				end_hour = resultSet.getString("end_hour");
				start_day = resultSet.getString("start_day");
				end_day = resultSet.getString("end_day");
				System.out.println(CompareDate(start_date,end_date,start_hour,end_hour,start_day,end_day));
				if (CompareDate(start_date,end_date,start_hour,end_hour,start_day,end_day))
					{awsoperation.awsStopInstance(ec2,instance_id);}
				else
					{awsoperation.awsStartInstance(ec2,instance_id);}
			}

			
		} catch (Exception e) {
			System.out.printf("StartStopOperation %s", e);
		}

		return "";

	}
	public Date getDate(String date,String hour,String day, Boolean reduce_minute ) throws ParseException {
		// TODO Auto-generated method stub
		Date result_date = null;
		try {
	      DateTimeFormatter formattere = DateTimeFormatter.ofPattern("EEEE");
	      TemporalAccessor accessor = formattere.parse(day); 
	      DayOfWeek day_of_week=DayOfWeek.from(accessor);

	      LocalDate currentdate = LocalDate.now();
	      int currentDay = currentdate.getDayOfMonth();
	      Month currentMonth = currentdate.getMonth();
	      int currentYear = currentdate.getYear();

	      Calendar cal = Calendar.getInstance();
	      LocalDate ld = LocalDate.of(currentYear, currentMonth, currentDay);
	      String week_date=null;
	      Calendar calenda = Calendar.getInstance();
		  Date current_date = calenda.getTime();
		  String today_day=new SimpleDateFormat("EEEE", Locale.ENGLISH).format(current_date.getTime());
		  if (today_day.equals(day))
			{
			DateFormat cur = new SimpleDateFormat("yyyy-MM-dd");  
			week_date = cur.format(current_date);  
			 
			}
		  else {
	      ld = ld.with(TemporalAdjusters.next(day_of_week));
	      DateTimeFormatter fo = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	      week_date = ld.format(fo);}

	      if (hour.contains(":"))
	      {hour=hour+":00";}
	      else
	      {hour=hour+":00:00";} 
	      
	      String final_date=week_date+" "+hour;
	      DateFormat dateFormatf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	      result_date = dateFormatf.parse(final_date);

	      cal.setTime(result_date);
	      if(reduce_minute)
	    	  cal.add(Calendar.MINUTE, -5);
	      else
		      cal.add(Calendar.MINUTE, 5);

	      result_date = cal.getTime();
		}
		catch (Exception e) {
			System.out.printf("get date and time %s", e);
		}
		return result_date;
	      
	}

	
	   public boolean CompareDate(String start_date, String end_date ,String start_hour, String end_hour,String start_day,String end_day) throws ParseException {
		   try {
	      Date f_start_date=getDate(start_date,start_hour,start_day,false);
		  Date f_end_date=getDate(end_date,end_hour,end_day,true);
			System.out.println(f_start_date);
			System.out.println(f_end_date);
			Date currentdate=new Date();
			
			DateFormat date_format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			start_date = start_date + " "+"00:00:00";
			end_date = end_date + " "+"23:55:00";
		     Date start_date_cycle = date_format.parse(start_date);
		     Date end_date_cycle = date_format.parse(end_date);
		    if(start_date_cycle.compareTo(currentdate) <=0 && end_date_cycle.compareTo(currentdate)>=0) {
		    if(f_start_date.compareTo(currentdate) <= 0 && f_end_date.compareTo(currentdate) >= 0) {
	         return true;
	         }
	         return false;
	      }
	      else {
		        System.out.println("Current Time Not exist between start and end schedule time");
	    	  return false;
	      }
	   
		   }
		   catch (Exception e) {
				System.out.printf("Compare date and time %s", e);
			}
		return false;
}
}

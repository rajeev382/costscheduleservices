package io.cloudensure.costschedule.dboperation;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;

public class Helper {

	/* Get actual class name to be printed on */
	static Logger logger = Logger.getLogger(Helper.class.getName());

	protected Logger getLogger() {
		return logger;

	}

	protected void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void info(String message) {
		logger.info(message);

	}

	public void debug(String message) {
		logger.debug(message);
	}

	public void error(String error) {
		logger.error(error);
	}

	public void error(String errorString, Exception e) {
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.error(String.format(errorString, exceptionAsString));
	}

}

package io.cloudensure.main;
import java.sql.SQLException;
import java.util.Timer;
import java.util.TimerTask;
import java.sql.Connection;
import io.cloudensure.costschedule.api.aws;
import io.cloudensure.costschedule.dboperation.dboperation;

public class main {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		final long timeInterval = 30000;
		dboperation obj= new dboperation();
		Connection connection = obj.connect();
		  Runnable runnable = new Runnable() {
			  public void run() {
				    while (true) {
				try {		
					obj.instanceStartStopOperation(connection);
					Thread.sleep(timeInterval);
				} catch (SQLException | InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    	} 
		}		
	};
		  Thread thread = new Thread(runnable);
		  thread.start();
	}
}


